package com.thanawat.app3.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.thanawat.app3.R
import com.thanawat.app3.model.Gas


class GasAdapter (private val context: Context, private val dataset: List<Gas>) :
    RecyclerView.Adapter<GasAdapter.GasViewHolder>() {
    class GasViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val gasPrice: TextView = view.findViewById(R.id.txtPrice)
        val gasName: TextView = view.findViewById(R.id.txtName)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GasViewHolder {
        val adapterLayout =
            LayoutInflater.from(parent.context).inflate(R.layout.list_gas, parent, false)
        return GasViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: GasViewHolder, position: Int) {
        val item = dataset[position]
        holder.gasPrice.text = item.price.toString()
        holder.gasName.text = item.name
        holder.itemView.setOnClickListener {
            Toast.makeText(
                context,
                item.price.toString() + " " + item.name,
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun getItemCount(): Int {
        return dataset.size
    }
}