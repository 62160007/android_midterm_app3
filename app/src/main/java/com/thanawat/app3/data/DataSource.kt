package com.thanawat.app3.data

import com.thanawat.app3.model.Gas

class DataSource {
    fun loadGas(): List<Gas> {
        return listOf<Gas> (
        Gas(
            "เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20",
            36.84,
        ),
        Gas(
            "เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91",
            37.68,
        ),
        Gas(
            "เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95",
            37.95,
        ),
        Gas(
            "เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95",
            45.44,
        ),
        Gas(
            "เชลล์ ดีเซล B20",
            36.34,
        ),
        Gas(
            "เชลล์ ฟิวเซฟ ดีเซล",
            36.34,
        ),
        Gas(
            "เชลล์ ฟิวเซฟ ดีเซล B7",
            36.34,
        ),
        Gas(
            "เชลล์ วี-เพาเวอร์ ดีเซล",
            36.34,
        ),
        Gas(
            "เชลล์ วี-เพาเวอร์ ดีเซล B7",
            47.06,
        ),
    )}
}
