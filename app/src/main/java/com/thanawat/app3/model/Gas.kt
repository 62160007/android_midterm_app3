package com.thanawat.app3.model
import androidx.annotation.DrawableRes

/**
 * A data class to represent the information presented in the dog card
 */
data class Gas(
    val name: String,
    val price: Double
)
